module.exports.counterFactory = function(){

    let x = 10;
    
    return {
        increment(){
            x = x + 1;
            return x;
        },
        decrement(){
            x = x - 1;
            return x;
        }
    }
};