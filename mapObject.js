module.exports.mapObject = function(obj, cb){

    for(let k in obj){
        let value = cb(obj[k], k);
        obj[k] = value;
    }
    return obj;
}