const cacheFunction = require('../cacheFunction');

function cb(n){
    console.log('executing cb function for ' + n + ' just once');
    return n*10;
}


const returnedFunction = cacheFunction.cacheFunction(cb);

console.log(returnedFunction(1));
console.log(returnedFunction(1));
console.log(returnedFunction(1));
console.log(returnedFunction(2));
console.log(returnedFunction(2));
console.log(returnedFunction(2));
