const limitFunctionCallCount = require('../limitFunctionCallCount');

function cb(){
    console.log('callback function is being invoked');
}

const returnedFunction = limitFunctionCallCount.limitFunctionCallCount(cb, 5);

returnedFunction();
returnedFunction();
returnedFunction();
returnedFunction();
returnedFunction();
returnedFunction();


