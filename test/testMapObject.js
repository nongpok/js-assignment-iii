const map = require('../mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function cb(value, key){
    return value + 5;
}

console.log(map.mapObject(testObject, cb));