module.exports.pairs = function(obj){

    let objList = new Array();
    for(let k in obj){
        let pair = new Array();
        pair.push(k);
        pair.push(obj[k]);
        objList.push(pair);
    }

    return objList;
}