module.exports.limitFunctionCallCount = function(cb, n){

    let count = 0;

    function returnedFunction(){
        if(count < n){
            count++;
            cb();
        }
        else{
            console.log('execution terminated after invoking the function ' + n + ' times' );
            return null;
        }
    }
    return returnedFunction;
};