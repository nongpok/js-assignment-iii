module.exports.defaults = function(obj, defaultProps){

    for(let k in defaultProps){
        if(obj[k] == undefined){
            let key = k;
            let value = defaultProps[k];
            obj[key] = value;
        }
    }
    return obj;
}