module.exports.cacheFunction = function(cb){

    let cache = {};

    function returnedFunction(...args){
        if(cache[args]){
            return cache[args];
        }
        cache[args] = cb(...args);
        return cache[args];
    }

    return returnedFunction;
};